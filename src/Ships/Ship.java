package Ships;

public class Ship implements ShipActions {
    int health;
    int attackPoints;
    int speed;
    int maneuverability;
    String name;
    String type;
    String specialAbility;
    boolean specialAbilityAvailable = true;

    Ship() {
    }

    public boolean retreat(int enemySpeed) {
        return speed > enemySpeed;
    }

    public void printShortShipInfo() {
        System.out.println("Ship's name: " + name + " || Class: " + type);
    }

    public void printLongShipInfo() {
        System.out.println("Ship's name: " + name + "\nClass: " + type + "\nHealth: " + health +
                "\nAttack points: " + attackPoints + "\nSpeed: " + speed);
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public boolean isSpecialAbilityAvailable() {
        return specialAbilityAvailable;
    }

    public int getHealth() {
        return health;
    }

    public String getName() {
        return name;
    }

    public int getSpeed() {
        return speed;
    }

    @Override
    public int attack() {
        return 0;
    }

    @Override
    public void takeDamage(int damageDone) {

    }

    @Override
    public int useSpecialAbility() {
        return 0;
    }

    @Override
    public boolean checkIfIsAlive() {
        return this.health > 0;
    }

}
