package Ships;

import java.util.Random;

public class Cruiser extends Ship {

    Cruiser() {
        super.type = "Cruiser";
        super.health = 60;
        super.speed = 30;
        super.maneuverability = 2;
        super.attackPoints = 40;
        super.name = selectRandomShipsName();
        super.specialAbility = "Initializing missile deflection ..";
    }

    private String selectRandomShipsName() {
        String[] shipNames = {"USS Port Royal", "Peter The Great", "USS Ticonderoga"};
        Random random = new Random();
        return shipNames[random.nextInt(shipNames.length)];
    }

    @Override
    public int attack() {
        int damageDone = (int) (Math.random() * super.attackPoints);
        System.out.println("Raining shells..");
        return damageDone;
    }

    @Override
    public void takeDamage(int damageDone) {
        int damageTaken;
        if (super.maneuverability > 0) {
            damageTaken = damageDone / super.maneuverability;
        } else {
            damageTaken = damageDone;
        }
        System.out.println(super.name + " on fire!! Damage taken: " + damageTaken);
        super.health -= damageTaken;
        System.out.println("Health left: " + super.health);
    }

    @Override
    public int useSpecialAbility() {
        System.out.println(super.specialAbility);
        System.out.println("You evade all your opponent's missiles and launch powerful attack.");
        super.specialAbilityAvailable = false;
        return super.attackPoints;
    }

}