package Ships;

import java.util.Random;

public class Destroyer extends Ship {

    Destroyer() {
        super.type = "Destroyer";
        super.health = 50;
        super.speed = 40;
        super.maneuverability = 3;
        super.attackPoints = 50;
        super.name = selectRandomShipsName();
        super.specialAbility = "Launching anti-missile shock wave ..";
    }

    private String selectRandomShipsName() {
        String[] shipNames = {"USS Zumwalt", "Nanchang", "Ashigara"};
        Random random = new Random();
        return shipNames[random.nextInt(shipNames.length)];
    }

    @Override
    public int attack() {
        int damageDone = (int) (Math.random() * super.attackPoints);
        System.out.println("Missile launch!");
        return damageDone;
    }

    @Override
    public void takeDamage(int damageDone) {
        int damageTaken;
        if (super.maneuverability > 0) {
            damageTaken = damageDone / super.maneuverability;
        } else {
            damageTaken = damageDone;
        }
        System.out.println(super.name + " hit by torpedo!! Damage taken: " + damageTaken);
        super.health -= damageTaken;
        System.out.println("Health left: " + super.health);
    }

    @Override
    public int useSpecialAbility() {
        System.out.println(super.specialAbility);
        System.out.println("Your opponent cannot move. Launching immediate attack.");
        super.specialAbilityAvailable = false;
        return super.attackPoints;
    }

}
