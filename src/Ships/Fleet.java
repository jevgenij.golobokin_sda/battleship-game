package Ships;

import java.util.*;

public class Fleet {

    private List<Ship> shipsInFleet;

    public Fleet() {
        this.shipsInFleet = createShipsFleet();
    }

    private List<Ship> createShipsFleet() {

        List<Ship> shipsInFleet = new ArrayList<>();

        shipsInFleet.add(new Cruiser());
        shipsInFleet.add(new Destroyer());
        shipsInFleet.add(new AircraftCarrier());

        Collections.shuffle(shipsInFleet);

        return shipsInFleet;
    }

    public void printOutShipsInFleet() {
        for (Ship ship : shipsInFleet) {
            ship.printShortShipInfo();
        }
    }

    public List<Ship> getShipsInFleet() {
        return shipsInFleet;
    }
}
