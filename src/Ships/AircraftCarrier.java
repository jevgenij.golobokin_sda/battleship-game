package Ships;

import java.util.Random;

public class AircraftCarrier extends Ship {

    AircraftCarrier() {
        super.type = "Aircraft Carrier";
        super.health = 100;
        super.speed = 25;
        super.maneuverability = 1;
        super.attackPoints = 65;
        super.name = selectRandomShipsName();
        super.specialAbility = "Launching all available aircrafts ..";
    }

    private String selectRandomShipsName() {
        String[] shipNames = {"USS George H.W. Bush", "HMS Queen Elizabeth", "Admiral Kuznetsov"};
        Random random = new Random();
        return shipNames[random.nextInt(shipNames.length)];
    }

    @Override
    public int attack() {
        int damageDone = (int) (Math.random() * super.attackPoints);
        System.out.println("Bombs away!");
        return damageDone;
    }

    @Override
    public void takeDamage(int damageDone) {
        int damageTaken;
        if (super.maneuverability > 0) {
            damageTaken = damageDone / super.maneuverability;
        } else {
            damageTaken = damageDone;
        }
        System.out.println(super.name + " got a hit!! Damage taken: " + damageTaken);
        super.health -= damageTaken;
        System.out.println("Health left: " + super.health);
    }

    @Override
    public int useSpecialAbility() {
        System.out.println(super.specialAbility);
        System.out.println("Full missile power attack!");
        super.specialAbilityAvailable = false;
        return super.attackPoints;
    }

}
