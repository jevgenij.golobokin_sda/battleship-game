package Ships;

public interface ShipActions {
    int attack();
    void takeDamage(int damageDone);
    int useSpecialAbility();
    boolean checkIfIsAlive();
}
