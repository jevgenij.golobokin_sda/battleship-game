import Ships.Ship;
import Ships.Fleet;

import java.util.Scanner;

public class GameLauncher {

    private Fleet userFleet;
    private Fleet enemyFleet;
    private int userScore;
    private int enemyScore;
    private String userInput;
    private final Scanner scanner = new Scanner(System.in);

    public void launchMenu() {
        System.out.println("Welcome to BATTLESHIP GAME !");
        System.out.println("Enter: \n1 to start game with 3 random ships in fleet. \n2 to exit");

        userInput = scanner.nextLine();

        switch (userInput) {
            case "1":
                launchRandomGame();
            case "2":
                System.exit(0);
            default:
                System.out.println("Invalid selection!");
                launchMenu();
        }
    }

    private void launchRandomGame() {
        //create user fleet
        userFleet = new Fleet();

        //create enemy fleet
        enemyFleet = new Fleet();

        System.out.println("Fleets created!");
        System.out.println("Your ships:");
        userFleet.printOutShipsInFleet();

        System.out.println("Enemy ships:");
        enemyFleet.printOutShipsInFleet();

        boolean selectionIsValid = false;
        while (!selectionIsValid) {

            System.out.println("---------------------------");

            System.out.println("1 - start battle! | 2 - randomize ships");
            userInput = scanner.nextLine();

            switch (userInput) {
                case "1":
                    selectionIsValid = true;
                    startBattle();

                    break;
                case "2":
                    selectionIsValid = true;
                    launchRandomGame();

                    break;
                default:
                    System.out.println("Selection invalid.");
            }
        }
    }

    private void startBattle() {
        int roundsCount = 3;
        userScore = 0;
        enemyScore = 0;

        while (roundsCount > 0) {

            for (int i = 0; i < 3; i++) {
                Ship userShipForThisRound = userFleet.getShipsInFleet().get(i);
                Ship enemyShipForThisRound = enemyFleet.getShipsInFleet().get(i);

                System.out.println("----------------------");
                System.out.println("Round " + (i + 1) + "!\n" +
                        "Your ship " + userShipForThisRound.getName() +
                        " vs. Enemy ship " + enemyShipForThisRound.getName());

                System.out.println("----------------------");
                System.out.println("Your ship:");
                userShipForThisRound.printLongShipInfo();

                System.out.println("----------------------");
                System.out.println("Enemy ship:");
                enemyShipForThisRound.printLongShipInfo();

                System.out.println("----------------------");
                System.out.println("Action controls: 1 - attack | 2 - use special ability (only once) | 3 - retreat");

                System.out.println("----------------------");

                fight(userShipForThisRound, enemyShipForThisRound);

                roundsCount--;
            }
        }

        printResult(userScore, enemyScore);
    }

    private void printResult(int userScore, int enemyScore) {
        System.out.println("---------------------");
        System.out.println("Result of this battle: \nYour score: " + userScore +
                "\nEnemy score: " + enemyScore);
        if (userScore > enemyScore) {
            System.out.println("---------------------");
            System.out.println("Victory !!!");
        } else if (userScore == enemyScore) {
            System.out.println("---------------------");
            System.out.println("Match ends in draw.");
        } else {
            System.out.println("---------------------");
            System.out.println("Defeat !!!");
        }
    }

    private void fight(Ship userShipForThisRound, Ship enemyShipForThisRound) {

        while (userShipForThisRound.checkIfIsAlive() && enemyShipForThisRound.checkIfIsAlive()) {
            int damageDone;

            System.out.println("### Your turn.");
            System.out.println("Select action:");

            userInput = scanner.nextLine();

            switch (userInput) {
                case "1":
                    damageDone = userShipForThisRound.attack();
                    enemyShipForThisRound.takeDamage(damageDone);

                    if (enemyShipForThisRound.getHealth() <= 0) {
                        System.out.println("Enemy destroyed!");
                        userScore++;
                        continue;
                    }

                    System.out.println("### Enemy turn!");
                    damageDone = enemyShipForThisRound.attack();
                    userShipForThisRound.takeDamage(damageDone);

                    if (userShipForThisRound.getHealth() <= 0) {
                        System.out.println("Your ship destroyed!");
                        enemyScore++;
                        continue;
                    }
                    break;

                case "2":
                    if (userShipForThisRound.isSpecialAbilityAvailable()) {
                        damageDone = userShipForThisRound.useSpecialAbility();
                        enemyShipForThisRound.takeDamage(damageDone);

                        if (enemyShipForThisRound.getHealth() <= 0) {
                            System.out.println("Enemy destroyed!");
                            userScore++;
                            continue;
                        }

                    } else {
                        System.out.println("Special ability already used! Select other option.");
                    }
                    break;

                case "3":
                    if (userShipForThisRound.retreat(enemyShipForThisRound.getSpeed())) {
                        System.out.println("Successfully retreated from battlefield!");
                        enemyShipForThisRound.setHealth(0);

                    } else {
                        System.out.println("You are too slow! Enemy got you and destroyed your ship!");
                        userShipForThisRound.setHealth(0);
                        enemyScore++;
                    }
                    break;
            }
        }
    }
}
